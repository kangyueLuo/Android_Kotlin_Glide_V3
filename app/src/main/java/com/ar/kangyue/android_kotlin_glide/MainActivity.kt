package com.ar.kangyue.android_kotlin_glide


import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.anthonycr.grant.PermissionsManager
import com.anthonycr.grant.PermissionsResultAction
import android.content.ComponentName
import android.content.Context.ACTIVITY_SERVICE
import android.app.ActivityManager
import android.content.Context
import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.fitCenter
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.centerCrop
import java.io.File


class MainActivity : AppCompatActivity() {

    private val TAG :String = "MainActivity"

    private var imageView1 : ImageView? =null
    private var imageView2 : ImageView?= null
    private var imageView3 : ImageView?= null
    private var imageView4 : ImageView? = null


    private var context : Context = this
    private val stockPhotoUrl :String = "https://cdn.pixabay.com/photo/2017/07/31/11/14/poppyseed-2557339_1280.jpg"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView1 = findViewById(R.id.glide_list_imageview1) as ImageView
        imageView2 = findViewById(R.id.glide_list_imageview2) as ImageView
        imageView3 = findViewById(R.id.glide_list_imageview3) as ImageView
        imageView4 = findViewById(R.id.glide_list_imageview4) as ImageView


        // 取得權限
        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this@MainActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), object : PermissionsResultAction(){
            override fun onGranted() {
                Toast.makeText(this@MainActivity, "READ_EXTERNAL_STORAGE Granted", Toast.LENGTH_SHORT).show()
            }

            override fun onDenied(permission: String?) {
               Toast.makeText(this@MainActivity, "Sorry, we need the Storage Permission to do that", Toast.LENGTH_SHORT).show()
            }
        })

        loadImageGif();
        loadImageLocalVideoThumbnail();

    }

    // 使用 url 載入圖片
    private fun loadImageByInternetUrl() {
        // the url could be any image URL, which is accessible with a normal HTTP GET request
        val internetUrl = "http://i.imgur.com/DvpvklR.png"

        Glide
                .with(context)
                .load(internetUrl)
                .into(imageView1)
    }

    // 載入 圖片id
    private fun loadImageByResourceId() {
        val resourceId = R.mipmap.ic_launcher

        Glide
                .with(context)
                .load(resourceId)
                .into(imageView2)
    }

    // 載入圖片檔案
    private fun loadImageByFile() {
        // this file probably does not exist on your device. However, you can use any file path, which points to an image file
        val file = File("/storage/emulated/0/Download/test01.png")

        Glide
                .with(context)
                .load<File>(file)
                .into(imageView3)
    }

    // 透過 Uri 載入圖片
    private fun loadImageByUri() {
        // this could be any Uri. for demonstration purposes we're just creating an Uri pointing to a launcher icon
        val uri = Uri.parse("android.resource://io.futurestud.glideyoutube/" + R.mipmap.ic_launcher)

        Glide
                .with(context)
                .load<Uri>(uri)
                .into(imageView4)
    }


    // 載入原圖
    private fun loadImageOriginal() {
        Glide
                .with(context)
                .load(stockPhotoUrl)
                .into(imageView1)
    }

    //
    private fun loadImageCenterCrop() {
        Glide
                .with(context)
                .load(stockPhotoUrl)
                .centerCrop()
                .into(imageView2)
    }

    // 載入後 已先碰到螢幕的編為準
    private fun loadImageFitCenter() {
        Glide
                .with(context)
                .load(stockPhotoUrl)
                .fitCenter()
                .into(imageView3)
    }

    // 載入後縮圖
    private fun loadImageWithResize() {
        Glide
                .with(context)
                .load(stockPhotoUrl)
                .override(400, 50)
                .into(imageView4)
    }

    private fun loadImagePlaceholder() {
        Glide
                .with(context)
                .load(stockPhotoUrl)
                .placeholder(R.drawable.hourglass)
                .error(R.drawable.false_icon)
                .fallback(R.drawable.futurestudio_logo)
                .into(imageView2)
    }

    private fun loadImageGif() {

        Glide
                .with(context)
                .load(R.drawable.testgif)
                .asGif()
                .placeholder(R.drawable.ic_cloud_off_red)
                .error(R.drawable.false_icon)
                .into(imageView1)
    }

    // need to put video to local storage and modify the path.
    private fun loadImageLocalVideoThumbnail() {
        Glide
                .with(context)
                .load("/storage/emulated/0/Download/Linda_MIX_2S.mp4")
                .error(R.drawable.false_icon)
                .into(imageView2)
    }

}
